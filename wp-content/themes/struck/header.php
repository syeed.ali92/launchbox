<?php
/**
 * Theme Header
 *
 * Displays all of the <head> section and everything up till <div id="sitecontainer">
 *
 * @package WordPress
 * @since 1.0
 */

global $tw_options;

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="<?php echo ('0' != $tw_options['ajax']) ? 'animsition-overlay' : 'no-animsition'; ?>" data-animsition-in-class="fade-in-anim" data-animsition-out-class="fade-out-anim" data-animsition-in-duration="400" data-animsition-out-duration="400">
	<?php do_action('tw_above_navigation'); ?>

	<?php get_template_part( TW_TEMPLATES_DIR . '/navigation' ); ?>

	<?php do_action('tw_below_navigation'); ?>

	<div id="sitecontainer">