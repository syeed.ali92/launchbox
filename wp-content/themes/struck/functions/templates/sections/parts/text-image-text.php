<?php
/**
 * The template for displaying text image section text.
 *
 * @package WordPress
 * @subpackage Struck
 * @since Struck 1.0
 */
$tag = 'h2';
if (is_page_template('template-sections-only.php')) {
    if ( $tw_row['is_slide'] ) {
        if ( $section_count == 1 && $slidenumber == 0 ) {
            $tag = 'h1';
        }
    } else {
        if ( $section_count == 1  ) {
            $tag = 'h1';
        } 
    }
}
?>

<div class="text-layout-inner">

    <?php if ( $tw_section['subtitle'] ) : ?>
        <div class="<?php echo sanitize_html_class( $tw_section['animations'] ); ?> subtitle tiny-details">
            <?php echo esc_html( $tw_section['subtitle'] ); ?>
        </div>
    <?php endif; ?>

    <?php if ( $tw_section['title'] ) : ?>
        <div class="<?php echo sanitize_html_class( $tw_section['animations'] ); ?> title">
           <?php echo "<$tag>" . esc_html( $tw_section['title'] ) . "</$tag>"; ?>
        </div>
    <?php endif; ?>

    <?php if ( $tw_section['title'] && $tw_section['content'] ) : ?>
        <hr class="<?php echo sanitize_html_class( $tw_section['animations'] ); ?> title-divider" data-width="100px" />
    <?php endif; ?>

    <?php if ( $tw_section['content'] ) : ?>
        <div class="<?php echo sanitize_html_class( $tw_section['animations'] ); ?> content">
            <?php echo apply_filters( 'the_content', $tw_section['content'] ); // run through the_content filter ?>
        </div>
    <?php endif; ?>

    <?php    
        // buttons output
        if ( locate_template( TW_SECTION_PARTS_DIR . 'buttons.php' ) ) {
            include( locate_template( TW_SECTION_PARTS_DIR . 'buttons.php' ) );
        }
    ?>

</div>