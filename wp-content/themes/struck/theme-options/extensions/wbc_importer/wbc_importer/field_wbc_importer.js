/* global redux_change, wp */

(function($) {
    "use strict";
    $.redux = $.redux || {};
    $(document).ready(function() {
        $.redux.wbc_importer();
    });
    $.redux.wbc_importer = function() {

        $('.wrap-importer.theme.not-imported, #wbc-importer-reimport').unbind('click').on('click', function(e) {

            // prevent default browser action
            e.preventDefault();

            // set defaults
            var parent 			= jQuery(this);
            var reimport 		= false;
            var contentFiles 	= []; // contentfiles array
            var message 		= 'Are you sure you want to import demo content? This is not reversible, which is why it\'s recommended for blank WordPress installs. Please note it will take several minutes to complete.';
            var $i 				= 0; // counter

            // check class of button to know if it's a reimport
            if (e.target.id == 'wbc-importer-reimport') {
                reimport = true;
                message = 'Re-Import Content?';

                if (!jQuery(this).hasClass('rendered')) {
                    parent = jQuery(this).parents('.wrap-importer');
                }
            }

            // if we're already imported and reimport is not hidden, return
            if (parent.hasClass('imported') && reimport == false) return;

            // confirm importation
            var r = confirm(message);
            if (r == false) return;

            // if reimporting, change classes accordingly
            if (reimport == true) {
                parent.removeClass('active imported').addClass('not-imported');
            }

            // show spinner hide button, set parent classes, fade thumbnail
            parent.find('.spinner').css('display', 'inline-block');
            parent.removeClass('active imported');
            parent.find('.importer-button').hide();
            parent.find('.wbc_image').css('opacity', '0.5');

            // get data associated with element
            var data = jQuery(this).data();

            // a static list of content files for now
            var contentFiles = [
                '00_images.xml',
                '01_posts.xml',
                '02_pages.xml',
                '03_portfolio.xml',
                '04_menu.xml'
            ];

            // get the length, initialize counter
            var filesCount = contentFiles.length;

            // set ajax actions
            data.action 			= "redux_wbc_importer";
            data.demo_import_id 	= parent.attr("data-demo-id");
            data.nonce 				= parent.attr("data-nonce");
            data.type 				= 'import-demo-content';
            data.last 				= false;
            data.wbc_import 		= (reimport == true) ? 're-importing' : ' ';

            importContent(); // import the content using the files

            // Function that imports content
            function importContent() {

                // set correct file to import
                data.file = contentFiles[$i];
                data.last = filesCount <= ($i + 1) ? true : false;

                jQuery.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: data,
                    beforeSend: function() {
                        console.log(data.file);
                        // update front end progress
                        parent.find('.spinner').html('Loading File ' + ( $i + 1 ) + ' of ' + filesCount);
                    },
                    success: function(data, textStatus, XMLHttpRequest) {

                        console.log(data);

                        if (data.length > 0 && data.match(/Have fun!/gi)) {

                            $i++; // increment couner

                            // if se still hav files
                            if ( filesCount > $i ){

                                importContent(); // run again

                            } else {

                                parent.find('.wbc_image').css('opacity', '1');
                                parent.find('.spinner').css('display', 'none');

                                if (reimport == false) {
                                    parent.addClass('rendered').find('.wbc-importer-buttons .importer-button').removeClass('import-demo-data');

                                    var reImportButton = '<div id="wbc-importer-reimport" class="wbc-importer-buttons button-primary import-demo-data importer-button">Re-Import</div>';
                                    parent.find('.theme-actions .wbc-importer-buttons').append(reImportButton);
                                }

                                parent.find('.importer-button:not(#wbc-importer-reimport)').removeClass('button-primary').addClass('button').text('Imported').show();
                                parent.find('.importer-button').attr('style', '');
                                parent.addClass('imported active').removeClass('not-imported');
                            }

                            // there are problems
                        } else {

                            parent.find('.import-demo-data').show();

                            if (reimport == true) {
                                parent.find('.importer-button:not(#wbc-importer-reimport)').removeClass('button-primary').addClass('button').text('Imported').show();
                                parent.find('.importer-button').attr('style', '');
                                parent.addClass('imported active').removeClass('not-imported');
                            }

                            alert('There was an error importing demo content: \n\n' + data.replace(/(<([^>]+)>)/gi, ""));
                        }

                    },
                    error: function(MLHttpRequest, textStatus, errorThrown) {
                        console.log(errorThrown);
                        console.log(MLHttpRequest);
                    },
                    complete: function(XMLHttpRequest, textStatus) {
                        console.log(textStatus);
                    },
                    //dataType: 'json'
                });

                // run the ajax request
                /* jQuery.post(ajaxurl, data, function(response) {

                 // on success
                 if (response.length > 0 && response.match(/Have fun!/gi)) {

                 $i++; // increment couner

                 // if se still hav files
                 if ( filesCount > $i ){

                 importContent(); // run again

                 } else {

                 parent.find('.wbc_image').css('opacity', '1');
                 parent.find('.spinner').css('display', 'none');

                 if (reimport == false) {
                 parent.addClass('rendered').find('.wbc-importer-buttons .importer-button').removeClass('import-demo-data');

                 var reImportButton = '<div id="wbc-importer-reimport" class="wbc-importer-buttons button-primary import-demo-data importer-button">Re-Import</div>';
                 parent.find('.theme-actions .wbc-importer-buttons').append(reImportButton);
                 }

                 parent.find('.importer-button:not(#wbc-importer-reimport)').removeClass('button-primary').addClass('button').text('Imported').show();
                 parent.find('.importer-button').attr('style', '');
                 parent.addClass('imported active').removeClass('not-imported');
                 }

                 // there are problems
                 } else {

                 parent.find('.import-demo-data').show();

                 if (reimport == true) {
                 parent.find('.importer-button:not(#wbc-importer-reimport)').removeClass('button-primary').addClass('button').text('Imported').show();
                 parent.find('.importer-button').attr('style', '');
                 parent.addClass('imported active').removeClass('not-imported');
                 }

                 alert('There was an error importing demo content: \n\n' + response.replace(/(<([^>]+)>)/gi, ""));
                 }
                 });*/

            }
            return false;
        });
    };
})(jQuery);